package main

// This command demonstrates the use of ghotscript to reduce the size
// of generated PDFs. This is based on a comment made by farkerhaiku:
// https://github.com/jung-kurt/gofpdf/issues/57#issuecomment-185843315

import (
	"fmt"
	"os"

	"github.com/jung-kurt/gofpdf"
)

func report(fileStr string, err error) {
	if err == nil {
		var info os.FileInfo
		info, err = os.Stat(fileStr)
		if err == nil {
			fmt.Printf("%s: OK, size %d\n", fileStr, info.Size())
		} else {
			fmt.Printf("%s: bad stat\n", fileStr)
		}
	} else {
		fmt.Printf("%s: %s\n", fileStr, err)
	}
}

func newPdf() (pdf *gofpdf.Fpdf) {

	//verse1 := "\ue001\u00a0In the beginning God created the heavens and the earth.  \ue002\u00a0The earth was formless and void, darkness was over the surface of the deep, and the Spirit of God was moving over the surface of the water."
	//verse3 := "\uf003\uf014\ue003\u00a0God said, “Let there be light,” and there was light.\uf001 \uf014\ue004\u00a0God saw that the light was good, and God separated the light from the darkness.\uf001 \uf014\ue005\u00a0God called the light Day, and the darkness He called Night. So the evening and the morning were the first day.\uf001"
	//verse6 := "\uf014\ue006\u00a0Then God said, “Let there be an expanse in the midst of the waters, and let it separate the waters from the waters.”\uf001 \uf014\ue007\u00a0So God made the expanse and separated the waters which were under the expanse from the waters which were above the expanse. And it was so.\uf001 \uf014\ue008\u00a0God called the expanse Heaven. So the evening and the morning were the second day.\uf001"
	//verse24 := "\uf014\ue002\ue004\u00a0Then God said, “Let the earth bring forth living creatures according to their kinds: livestock, and creeping things, and beasts of the earth according to their kinds.” And it was so.\uf001\uf014\ue002\ue005\u00a0So God made the beasts of the earth according to their kind, and the livestock according to their kind, and everything that creeps on the earth according to its kind. And God saw that it was good.\uf001"
	verse22 := "\uf007\uf014\ue002\u00a0The sons of Japheth were\uf001\uf001\uf003\uf007\uf007Gomer, Magog, Madai, Javan, Tubal, Meshek, and Tiras.\uf001\uf001\uf012a\uf001"

	//pdf = gofpdf.New("P", "mm", "A4", "./")
	pdf = gofpdf.NewCustom(&gofpdf.InitType{
		UnitStr: "in",
		Size:    gofpdf.SizeType{Wd: 7.5, Ht: 10.0},
	})
	pdf.SetCompression(false)
	pdf.AddUTF8Font("Times", "I", "Times New Roman Italic.ttf")
	pdf.AddUTF8Font("Times", "B", "Times New Roman Bold.ttf")
	pdf.AddUTF8Font("Times", "", "Times New Roman Regular.ttf")
	pdf.AddUTF8Font("Rockwell", "", "RockwellStd-Light.ttf")
	pdf.AddPage()

	pdf.SetFont("Times", "I", 12)
	pdf.SetTextColor(255, 255, 255)
	pdf.MultiCell(0, 0.41, "\u00a0 _,-+`\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", "", "", false)

	pdf.SetFont("Times", "B", 12)
	pdf.SetTextColor(255, 255, 255)
	pdf.MultiCell(0, 0.41, "\u00a0 _,-+`\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", "", "", false)

	pdf.SetTextColor(0, 0, 0)
	pdf.SetFont("Rockwell", "", 10)
	_, h := pdf.GetFontSize()
	//pdf.RAIMultiCell(5.25, h*1.25, verse1, "LTRB", "", false)
	//pdf.RAIMultiCell(5.25, h*1.25, verse3, "LTRB", "", false)
	//pdf.RAIMultiCell(5.25, h*1.25, verse6, "LTRB", "", false)
	pdf.RAIMultiCell(5.25, h*1.25, verse22, "LTRB", "", false)
	//	pdf.RAIMultiCell(5.25, h*1.25, gen2, "LTRB", "", false)
	//pdf.RedMultiCell(0, h*1.25, s2, "", "", false)

	//pdf.SetFont("Times", "I", 12)
	//pdf.RedMultiCell(0, h*1.25, s, "", "", false)
	//pdf.RedMultiCell(0, h*1.25, s2, "", "", false)

	//pdf.RedMultiCell(0, h*1.25, s2, "", "", false)

	return
}

func simplePdf() (pdf *gofpdf.Fpdf) {

	//s := "This is \uF006a test string \uF001\uf004\uf006to \uf001\uf001\uf006run an take cover and test the word wrap across multiple lines and see if we can get this going perfectly\uf001 of course.  Dont take our \uf006word for it, lets test \uf001it here."
	// s2 := "This is a test string \uf012\uf004\uf006to \uf001\uf001\uf006run an take cover and test the word wrap across multiple lines and see if we can get this going perfectly\uf001 of course.  Dont take our \uf006word for it, lets test \uf001it here."
	//s2 := "Ok this is the test of your life. This is a test string I would run and \uF00Ctake\uF001 cover and keep going see what happens, we need to just keep going \uF002\uf015\uf007\uf014And going and going and going and going to wrap a few lines test test2 test3 test4 test5 test6 test7 test8 asdf\uf001\uf001\uf001\uf001\uf003\uf015\uf007\uf007the word wrap across multiple lines and see if we can get this going perfectly of course.  \uf001\uf001\uf001\uf001\uf003\uf015\uf007\uf007Dont take our \uf006word for it, lets test \uf001it here."
	//s2 := "To run \uf004run \uf001an take cover and test the Jump Shot"

	s2 := "However, when \uF006quotations from the \uF004MEV text\uF001 are used in church bulletins, \uF001orders of following notice may be used at the end of each quotation: MEV."

	//pdf = gofpdf.New("P", "mm", "A4", "./")
	pdf = gofpdf.NewCustom(&gofpdf.InitType{
		UnitStr: "in",
		Size:    gofpdf.SizeType{Wd: 7.5, Ht: 10.0},
	})
	pdf.SetCompression(false)
	pdf.AddUTF8Font("Times", "", "Times New Roman Regular.ttf")
	pdf.AddUTF8Font("Times", "I", "Times New Roman Italic.ttf")
	pdf.AddUTF8Font("Times", "B", "Times New Roman Bold.ttf")
	pdf.SetFont("Times", "I", 12)
	pdf.SetTextColor(255, 255, 255)
	pdf.MultiCell(0, 0.41, " _,-+`\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", "", "", false)

	pdf.AddPage()

	pdf.SetTextColor(0, 0, 0)
	pdf.SetFont("Times", "", 10)
	_, h := pdf.GetFontSize()
	pdf.RAIMultiCell(5.25, h*1.25, s2, "LTRB", "", false)
	return
}

func main() {
	report("test.pdf", newPdf().OutputFileAndClose("test.pdf"))
	// report("test2.pdf", simplePdf().OutputFileAndClose("test2.pdf"))
}
