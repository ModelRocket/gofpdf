package gofpdf

import "github.com/kr/pretty"

const (
	// FormatMarkerClear clears formatting
	FormatMarkerClear rune = '\uF000' + iota
	// FormatMarkerEnd denotes the end of the previous formatting
	FormatMarkerEnd
	// FormatMarkerLineBreak is equivalent to <br/> in HTML
	FormatMarkerLineBreak
	// FormatMarkerParagraph is equivalent to <p> in HTML
	FormatMarkerParagraph
	// FormatMarkerItalic is equivalent to <i> in HTML
	FormatMarkerItalic
	// FormatMarkerSmallCaps denotes that the string should be small caps
	FormatMarkerSmallCaps
	// FormatMarkerDomincal is used to mark the words of Jesus Christ
	FormatMarkerDomincal
	// FormatMarkerIndent marks an indented section
	FormatMarkerIndent
	// FormatMarkerAllCaps marks text that should be all caps
	FormatMarkerAllCaps
	// FormatMarkerSuperscript marks a superscript text section
	FormatMarkerSuperscript
	// FormatMarkerSubscript marks a subscript text section
	FormatMarkerSubscript
	// FormatMarkerUnderline is equivalent to <u> in HTML
	FormatMarkerUnderline
	// FormatMarkerBold is equivalent to <b> in HTML
	FormatMarkerBold
	// FormatMarkerTitle marks a title section of text, <h1>
	FormatMarkerTitle
	// FormatMarkerHeading marks a heading or <h2>
	FormatMarkerHeading
	// FormatMarkerSubheading marks a sub-heading or <h3>
	FormatMarkerSubheading
	// FormatMarkerPoetry denote a section that would be considered poetry
	FormatMarkerPoetry
	// FormatMarkerFootnote marks a footnote positional hint
	FormatMarkerFootnote
	// FormatMarkerCrossRef marks a cross reference positional hint
	FormatMarkerCrossRef
	// FormatMarkerVerse marks a new verse
	FormatMarkerVerse
	// FormatMarkerHangingIndent is a new line with indent
	FormatMarkerHangingIndent
	// FormatMarkerList is a list
	FormatMarkerList
	// FormatMarkerTab is the tab at the start of a paragraph
	FormatMarkerTab
	// FormatMarkerOSIS marks an OSIS identifier tag i.e. Gen.1.1
	FormatMarkerOSIS
	// FormatMarkerRGB marks a color tag, the next three runes will have RGB data encoded
	// Red: U+F300-F3FF
	// Green: U+F400-F3FF
	// Blue: U+F500-F5FF
	FormatMarkerRGB
)

const (
	// FormatMarkerClearInt clears formatting
	FormatMarkerClearInt int = 61440 + iota
	// FormatMarkerEndInt denotes the end of the previous formatting
	FormatMarkerEndInt
	// FormatMarkerLineBreakInt is equivalent to <br/> in HTML
	FormatMarkerLineBreakInt
	// FormatMarkerParagraphInt is equivalent to <p> in HTML
	FormatMarkerParagraphInt
	// FormatMarkerItalicInt is equivalent to <i> in HTML
	FormatMarkerItalicInt
	// FormatMarkerSmallCapsInt denotes that the string should be small caps
	FormatMarkerSmallCapsInt
	// FormatMarkerDomincalInt is used to mark the words of Jesus Christ
	FormatMarkerDomincalInt
	// FormatMarkerIndentInt marks an indented section
	FormatMarkerIndentInt
	// FormatMarkerAllCapsInt marks text that should be all caps
	FormatMarkerAllCapsInt
	// FormatMarkerSuperscriptInt marks a superscript text section
	FormatMarkerSuperscriptInt
	// FormatMarkerSubscriptInt marks a subscript text section
	FormatMarkerSubscriptInt
	// FormatMarkerUnderlineInt is equivalent to <u> in HTML
	FormatMarkerUnderlineInt
	// FormatMarkerBoldInt is equivalent to <b> in HTML
	FormatMarkerBoldInt
	// FormatMarkerTitleInt marks a title section of text, <h1>
	FormatMarkerTitleInt
	// FormatMarkerHeadingInt marks a heading or <h2>
	FormatMarkerHeadingInt
	// FormatMarkerSubheadingInt marks a sub-heading or <h3>
	FormatMarkerSubheadingInt
	// FormatMarkerPoetryInt denote a section that would be considered poetry
	FormatMarkerPoetryInt
	// FormatMarkerFootnoteInt marks a footnote positional hint
	FormatMarkerFootnoteInt
	// FormatMarkerCrossRefInt marks a cross reference positional hint
	FormatMarkerCrossRefInt
	// FormatMarkerVerseInt marks a new verse hint
	FormatMarkerVerseInt
	// FormatMarkerOSISInt marks an OSIS identifier tag i.e. Gen.1.1
	FormatMarkerOSISInt
	// FormatMarkerHangingIndentInt is a hanging indent
	FormatMarkerHangingIndentInt
	// FormatMarkerListInt is a list
	FormatMarkerListInt
	// FormatMarkerTabInt
	FormatMarkerTabInt

	// FormatMarkerRGBInt marks a color tag, the next three runes will have RGB data encoded
	// Red: U+F300-F3FF
	// Green: U+F400-F3FF
	// Blue: U+F500-F5FF
	FormatMarkerRGBInt
)
const (
	//PrivateUseStart beginning of the Private Use Area Characters we use for Hints
	// https://en.wikipedia.org/wiki/Private_Use_Areas
	PrivateUseStart = '\uE000'
	// PrivateUseAlphaStart is the start of the Alpha Characters in the Private Use Areas
	PrivateUseAlphaStart = '\uE00A'
	// PrivateUseNoWidthMin anything above this character needs to have ZERO width
	PrivateUseNoWidthMin = FormatMarkerClear
	// PrivateUseEnd is the last private use character
	PrivateUseEnd = '\uF8FF'
)

type (
	// FormatMarker is a string representation of a unicode rune
	FormatMarker rune

	// FormatMarkerInt is the integer value of the FormatMarkers
	FormatMarkerInt int
)

func debugString(s string) {
	for i, r := range s {
		c1 := byte(s[i])
		if c1 >= 224 {
			c2 := byte(s[i+1])
			c3 := byte(s[i+2])
			pretty.Logf("%q,%d,%d,%d,%d", r, i, c1, c2, c3)
		} else {
			pretty.Logf("%q,%d,%d", r, i, c1)
		}
	}
}
