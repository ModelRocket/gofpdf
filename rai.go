package gofpdf

import (
	"fmt"
	"math"
	"strings"

	"github.com/kr/pretty"
)

func (f *Fpdf) popIndentStack() {
	for i := len(f.indentStack) - 1; i >= 0; i-- {
		f.indentStack[i]()
	}
	f.indentStack = f.indentStack[:0]
}
func debugString2(s string) {
	str := ""
	for _, r := range s {
		str += fmt.Sprintf("%q", r)
	}
	pretty.Log(strings.ReplaceAll(str, "'", ""))
}

func (f *Fpdf) SetRedLetterOn(b bool) {
	if b {
		f.redValue = RedLetterOn
	} else {
		f.redValue = RedLetterOff
	}
}

// RAIMultiCell supports printing text with line breaks. They can be automatic (as
// soon as the text reaches the right border of the cell) or explicit (via the
// \n character). As many cells as necessary are output, one below the other.
//
// Text can be aligned, centered or justified. The cell block can be framed and
// the background painted. See CellFormat() for more details.
//
// The current position after calling MultiCell() is the beginning of the next
// line, equivalent to calling CellFormat with ln equal to 1.
//
// w is the width of the cells. A value of zero indicates cells that reach to
// the right margin.
//
// h indicates the line height of each cell in the unit of measure specified in New().
//
// Note: this method has a known bug that treats UTF-8 fonts differently than
// non-UTF-8 fonts. With UTF-8 fonts, all trailing newlines in txtStr are
// removed. With a non-UTF-8 font, if txtStr has one or more trailing newlines,
// only the last is removed. In the next major module version, the UTF-8 logic
// will be changed to match the non-UTF-8 logic. To prepare for that change,
// applications that use UTF-8 fonts and depend on having all trailing newlines
// removed should call strings.TrimRight(txtStr, "\r\n") before calling this
// method.
func (f *Fpdf) RAIMultiCell(w, h float64, txtStr, borderStr, alignStr string, fill bool) {
	if f.err != nil {
		return
	}
	// dbg("MultiCell")
	if alignStr == "" {
		alignStr = "J"
	}
	cw := f.currentFont.Cw
	if w == 0 {
		w = f.w - f.rMargin - f.x
	}
	wmax := int(math.Ceil((w - 2*f.cMargin) * 1000 / f.fontSize))
	s := strings.Replace(txtStr, "\r", "", -1)
	// The logic for indenting assumes the Hanging indent character is first, if we get it second it breaks, so clean up
	s = strings.Replace(s, "\uf007\uf014", "\uf014\uf007", -1)
	//s = strings.Replace(s, "\uf007\uf007", "\uf007", -1)

	srune := []rune(s)
	//debugString2(txtStr)

	// remove extra line breaks
	var nb int
	nb = len(srune)
	for nb > 0 && srune[nb-1] == '\n' {
		nb--
	}
	srune = srune[0:nb]

	// dbg("[%s]\n", s)
	var b, b2 string
	b = "0"
	if len(borderStr) > 0 {
		if borderStr == "1" {
			borderStr = "LTRB"
			b = "LRT"
			b2 = "LR"
		} else {
			b2 = ""
			if strings.Contains(borderStr, "L") {
				b2 += "L"
			}
			if strings.Contains(borderStr, "R") {
				b2 += "R"
			}
			if strings.Contains(borderStr, "T") {
				b = b2 + "T"
			} else {
				b = b2
			}
		}
	}
	sep := -1
	i := 0 // current index into the srune array
	j := 0 // last index of i for the beginning of the line
	l := 0
	ls := 0
	ns := 0
	nl := 1
	for i < nb {
		// Get next character
		var c rune
		c = srune[i]

		if c == rune(FormatMarkerHangingIndent) {
			// XXX WILL THIS ALWAYS BE AT A NEW LINE
			i++
			if f.indent == 0 {
				f.indent = 0.25
				f.x += f.indent
				wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
				f.indentStack = append(f.indentStack, func() {
					if f.indent == 0.25 {
						f.x += 0.4 - f.indent
						f.indent = 0.4
						wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
					}
					// Else keep it as is
				})
			} else {
				f.indentStack = append(f.indentStack, func() {})
			}
			continue
		} else if c == rune(FormatMarkerPoetry) {
			i++
			if f.indent == 0 {
				f.indent = 0.45
				f.x += f.indent
				wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
				f.indentStack = append(f.indentStack, func() {
					f.x -= f.indent
					f.indent = 0
					wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
				})
			} else {
				f.indentStack = append(f.indentStack, func() {})
			}
			continue
		} else if c == rune(FormatMarkerIndent) {
			i++
			if f.indent == 0.45 { // In Poetry - subsequent line
				f.indent = 0.60
				f.x += 0.15
				wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
				f.indentStack = append(f.indentStack, func() {
					f.x -= 0.15
					f.indent = 0.45
					wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
				})
			} else if f.indent == 0 {
				f.indent = 0.4
				f.x += f.indent
				wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
				f.indentStack = append(f.indentStack, func() {})
			} else {
				f.indentStack = append(f.indentStack, func() {})
			}
			continue
		} else if c == rune(FormatMarkerTab) && f.indent == 0 {
			f.indent = 0.25
			f.x += f.indent
			i++
			wmax = int(math.Ceil((w - f.indent - 2*f.cMargin) * 1000 / f.fontSize))
			f.indentStack = append(f.indentStack, func() {
				f.x -= f.indent
				f.indent = 0
				wmax = int(math.Ceil((w - 2*f.cMargin) * 1000 / f.fontSize))
			})
			continue
		} else if c == '\n' || c == rune(FormatMarkerLineBreak) || c == rune(FormatMarkerParagraph) {
			// Explicit line break
			if f.ws > 0 {
				f.ws = 0
				f.out("0 Tw")
			}

			newAlignStr := alignStr
			if newAlignStr == "J" {
				if f.isRTL {
					newAlignStr = "R"
				} else {
					newAlignStr = "L"
				}
			}
			f.RAICellFormat(w, h, string(srune[j:i]), b, 2, newAlignStr, fill, 0, "")
			i++
			sep = -1
			j = i
			l = 0
			ns = 0
			nl++
			if len(borderStr) > 0 && nl == 2 {
				b = b2
			}
			continue
		}
		if c == ' ' || isChinese(c) {
			sep = i
			ls = l
			ns++
		}
		if int(c) >= len(cw) {
			f.err = fmt.Errorf("character outside the supported range: %s", string(c))
			return
		}
		if cw[int(c)] == 0 && c < PrivateUseNoWidthMin { //Marker width 0 used for missing symbols
			l += f.currentFont.Desc.MissingWidth
		} else if cw[int(c)] != 65535 { //Marker width 65535 used for zero width symbols
			l += cw[int(c)]
		}
		if l > wmax {
			// Automatic line break
			if sep == -1 {
				if i == j {
					i++
				}
				if f.ws > 0 {
					f.ws = 0
					f.out("0 Tw")
				}
				f.RAICellFormat(w, h, string(srune[j:i]), b, 2, alignStr, fill, 0, "")
			} else {
				if alignStr == "J" {
					if ns > 1 {
						f.ws = float64((wmax-ls)/1000) * f.fontSize / float64(ns-1)
					} else {
						f.ws = 0
					}
					f.outf("%.3f Tw", f.ws*f.k)
				}
				f.RAICellFormat(w, h, string(srune[j:sep]), b, 2, alignStr, fill, 0, "")
				i = sep + 1
			}
			sep = -1
			j = i
			l = 0
			ns = 0
			nl++
			if len(borderStr) > 0 && nl == 2 {
				b = b2
			}
		} else {
			i++
		}
	}
	// Last chunk
	if f.ws > 0 {
		f.ws = 0
		f.out("0 Tw")
	}
	if len(borderStr) > 0 && strings.Contains(borderStr, "B") {
		b += "B"
	}
	if alignStr == "J" {
		if f.isRTL {
			alignStr = "R"
		} else if wmax-l > 5000 || (wmax-l > 2500 && srune[i-1] == '.') {
			alignStr = ""
		}
	}
	f.RAICellFormat(w, h, string(srune[j:i]), b, 2, alignStr, fill, 0, "")
	f.x = f.lMargin
	f.indent = 0
	// Reset the indent after the section has been rendered
	//if f.indent > 0 {
	//	f.x -= f.indent
	//}

}

// RAICellFormat is the RAI specific cell format that uses the markup in
func (f *Fpdf) RAICellFormat(w, h float64, txtStr, borderStr string, ln int,
	alignStr string, fill bool, link int, linkStr string) {
	// This is a shortcut.  Basically if we are indenting and the line does not end with a terminator, we wait until the next line to pop
	if strings.HasSuffix(txtStr, string(FormatMarkerEnd)) || strings.HasPrefix(strings.TrimSpace(txtStr), string(FormatMarkerTab)) {
		defer f.popIndentStack()
	}
	// dbg("CellFormat. h = %.2f, borderStr = %s", h, borderStr)
	if f.err != nil {
		return
	}
	if f.currentFont.Name == "" {
		f.err = fmt.Errorf("font has not been set; unable to render text")
		return
	}
	if len(txtStr) == 0 {
		f.x -= f.indent
		f.indent = 0
		f.indentStack = f.indentStack[:0]
	}
	//debugString2(txtStr)
	w -= f.indent
	borderStr = strings.ToUpper(borderStr)
	k := f.k
	var s fmtBuffer
	if fill || borderStr == "1" {
		var op string
		if fill {
			if borderStr == "1" {
				op = "B"
				// dbg("border is '1', fill")
			} else {
				op = "f"
				// dbg("border is empty, fill")
			}
		} else {
			// dbg("border is '1', no fill")
			op = "S"
		}
		/// dbg("(CellFormat) f.x %.2f f.k %.2f", f.x, f.k)
		s.printf("%.2f %.2f %.2f %.2f re %s ", f.x*k, (f.h-f.y)*k, w*k, -h*k, op)
	}
	if len(borderStr) > 0 && borderStr != "1" {
		// fmt.Printf("border is '%s', no fill\n", borderStr)
		x := f.x
		y := f.y
		left := x * k
		top := (f.h - y) * k
		right := (x + w) * k
		bottom := (f.h - (y + h)) * k
		if strings.Contains(borderStr, "L") {
			s.printf("%.2f %.2f m %.2f %.2f l S ", left, top, left, bottom)
		}
		if strings.Contains(borderStr, "T") {
			s.printf("%.2f %.2f m %.2f %.2f l S ", left, top, right, top)
		}
		if strings.Contains(borderStr, "R") {
			s.printf("%.2f %.2f m %.2f %.2f l S ", right, top, right, bottom)
		}
		if strings.Contains(borderStr, "B") {
			s.printf("%.2f %.2f m %.2f %.2f l S ", left, bottom, right, bottom)
		}
	}
	if len(txtStr) > 0 {
		var dx, dy float64
		// Horizontal alignment
		switch {
		case strings.Contains(alignStr, "R"):
			dx = w - f.cMargin - f.GetStringWidth(txtStr)
		case strings.Contains(alignStr, "C"):
			dx = (w - f.GetStringWidth(txtStr)) / 2
		default:
			dx = f.cMargin
		}

		// Vertical alignment
		switch {
		case strings.Contains(alignStr, "T"):
			dy = (f.fontSize - h) / 2.0
		case strings.Contains(alignStr, "B"):
			dy = (h - f.fontSize) / 2.0
		case strings.Contains(alignStr, "A"):
			var descent float64
			d := f.currentFont.Desc
			if d.Descent == 0 {
				// not defined (standard font?), use average of 19%
				descent = -0.19 * f.fontSize
			} else {
				descent = float64(d.Descent) * f.fontSize / float64(d.Ascent-d.Descent)
			}
			dy = (h-f.fontSize)/2.0 - descent
		default:
			dy = 0
		}
		for _, uni := range []rune(txtStr) {
			f.currentFont.usedRunes[int(uni)] = int(uni)
		}

		shift := 0.0
		space := f.escape(utf8toutf16(" ", false))
		bt := (f.x + dx) * k
		//td := (f.h - (f.y + dy + .5*h + .3*f.fontSize)) * k
		td := (f.h - (f.y + .5*h + .3*f.fontSize)) * k

		t := strings.Split(txtStr, " ")

		if alignStr == "J" || f.ws != 0 {
			wmax := int(math.Ceil((w - 2*f.cMargin) * 1000 / f.fontSize))
			strSize := f.GetStringSymbolWidth(txtStr)
			shift = float64((wmax - strSize)) / float64(len(t)-1)

		}

		// HERE IS WHERE WE WRITE THE TEXT....

		//If multibyte, Tw has no effect - do word spacing using an adjustment before each space
		s.printf("BT 0 Tw %.2f %.2f Td [", bt, td)
		numt := len(t)
		for i := 0; i < numt; i++ {
			tx := t[i]
			tx2 := ""
			var lastChar rune = 0
			for _, c := range tx {
				// This pops off the stack
				if c == FormatMarkerEnd {
					if len(f.formatStack) > 0 {
						n := len(f.formatStack) - 1
						if len(tx2) > 0 {
							s.printf("(%s)", f.escape(utf8toutf16(tx2, false)))
							tx2 = ""
						}
						s.printf(f.formatStack[n])
						f.formatStack = f.formatStack[:n]
					}
					// The Indents
				} else if c == FormatMarkerIndent || c == FormatMarkerPoetry || c == FormatMarkerHangingIndent || c == FormatMarkerTab || c == FormatMarkerParagraph {
					continue
				} else if c == FormatMarkerFootnote {
					s.printf("(%s)", f.escape(utf8toutf16(tx2, false)))
					tx2 = ""
					f.formatStack = append(f.formatStack, fmt.Sprintf("] TJ 0 Ts /F%s %.2f Tf [", f.currentFont.i, f.fontSizePt))
					fontKey := f.fontFamily + "B"
					font := f.fonts[fontKey]
					s.printf("] TJ 4 Ts /F%s %.2f Tf [", font.i, f.fontSizePt*0.66)
				} else if c == FormatMarkerCrossRef {
					if len(tx2) > 0 && lastChar < PrivateUseAlphaStart {
						s.printf("(%s)", f.escape(utf8toutf16(tx2, false)))
						tx2 = ""
					} else if lastChar >= PrivateUseAlphaStart {
						s.printf("(%s)", f.escape(utf8toutf16(strings.TrimRight(tx2, string(lastChar)), false)))
						tx2 = string(lastChar)
					}
					f.formatStack = append(f.formatStack, fmt.Sprintf("] TJ 0 Ts /F%s %.2f Tf [", f.currentFont.i, f.fontSizePt))
					fontKey := f.fontFamily + "I"
					font := f.fonts[fontKey]
					s.printf("] TJ 4 Ts /F%s %.2f Tf [", font.i, f.fontSizePt*0.66)
				} else if c == FormatMarkerDomincal {
					if len(tx2) > 0 && lastChar < PrivateUseAlphaStart {
						s.printf("(%s)", f.escape(utf8toutf16(tx2, false)))
						tx2 = ""
					} else if lastChar >= PrivateUseAlphaStart {
						s.printf("(%s)", f.escape(utf8toutf16(strings.TrimRight(tx2, string(lastChar)), false)))
						tx2 = string(lastChar)
					}

					f.formatStack = append(f.formatStack, "] TJ 0.0 0.0 0.0 rg [")
					s.printf("] TJ %f 0.0 0.0 rg [", f.redValue)
				} else if c == FormatMarkerItalic || c == FormatMarkerBold {
					f.formatStack = append(f.formatStack, fmt.Sprintf("] TJ /F%s %.2f Tf [", f.currentFont.i, f.fontSizePt))
					if len(tx2) > 0 {
						s.printf("(%s)", f.escape(utf8toutf16(tx2, false)))
						tx2 = ""
					}
					fontKey := f.fontFamily + "I"
					if c == FormatMarkerBold {
						fontKey = f.fontFamily + "B"
					}
					font := f.fonts[fontKey]
					s.printf("] TJ /F%s %.2f Tf [", font.i, f.fontSizePt)
				} else if c == FormatMarkerVerse {
				} else if c == FormatMarkerList {
					f.formatStack = append(f.formatStack, "")
				} else {
					tx2 += string(c)
					lastChar = c
				}
			}
			tx = "(" + f.escape(utf8toutf16(tx2, false)) + ")"
			s.printf("%s ", tx)
			if (i + 1) < numt {
				s.printf("%.3f(%s) ", -shift, space)
			}
		}
		s.printf("] TJ ET")

		// DONE WRITING TEXT
		if f.underline {
			s.printf(" %s", f.dounderline(f.x+dx, f.y+dy+.5*h+.3*f.fontSize, txtStr))
		}
		if f.strikeout {
			s.printf(" %s", f.dostrikeout(f.x+dx, f.y+dy+.5*h+.3*f.fontSize, txtStr))
		}
		if f.colorFlag {
			s.printf(" Q")
		}
		if link > 0 || len(linkStr) > 0 {
			f.newLink(f.x+dx, f.y+dy+.5*h-.5*f.fontSize, f.GetStringWidth(txtStr), f.fontSize, link, linkStr)
		}
	}
	str := s.String()
	if len(str) > 0 {
		f.out(str)
	}
	f.lasth = h
	if ln > 0 {
		// Go to next line
		f.y += h
		if ln == 1 {
			f.x = f.lMargin
		}
	} else {
		f.x += w
	}
	return
}
